# Api Clinica
 
## Como usar?

1 - Clone o repositório com __git clone__;

2 - No terminal, copie o __.env.example__ para __.env__;

3 - Configure seu banco de dados em __.env__;

4 - Execute __composer install__;

5 - Execute __php artisan key:generate__;

6 - Execute __php artisan migrate --seed__;

7 - ✅ Execute __php artisan serve --port=8000__;