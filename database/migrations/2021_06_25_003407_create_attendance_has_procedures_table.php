<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceHasProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_has_procedures', function (Blueprint $table) {
            $table->unsignedInteger('attendance_id');
            $table->unsignedInteger('procedure_id');

            $table->primary(['attendance_id', 'procedure_id']);

            //FK
            $table->foreign('attendance_id')->references('id')->on('attendances');
            //FK
			$table->foreign('procedure_id')->references('id')->on('procedures');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_has_procedures');
    }
}
