<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalHasProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_has_procedures', function (Blueprint $table) {
            $table->unsignedInteger('professional_id');
            $table->unsignedInteger('procedure_id');

            $table->primary(['professional_id', 'procedure_id']);

            //FK
            $table->foreign('professional_id')->references('id')->on('professionals');
            //FK
			$table->foreign('procedure_id')->references('id')->on('procedures');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_has_procedures');
    }
}
