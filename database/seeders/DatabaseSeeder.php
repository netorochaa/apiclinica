<?php

namespace Database\Seeders;

use App\Models\Procedure;
use App\Models\Professional;
use App\Models\ProfessionalHasProcedure;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createProfessionals();
        $this->createProcedures();
    }

    public function createProfessionals()
    {
        Professional::create([
            'name' => 'Drº Drauzio',
            'speciality' => 'Cardiologia',
            'commission' => '40'
        ]);

        Professional::create([
            'name' => 'Drº Henrique',
            'speciality' => 'Otorrinolaringologia',
            'commission' => '20'
        ]);

        Professional::create([
            'name' => 'Drº David',
            'speciality' => 'Ortopedia',
            'commission' => '30'
        ]);
    }

    public function createProcedures()
    {
        // Cardiologia
        Procedure::create([
            'name' => 'Eletrocardiograma',
            'value' => '200,00'
        ]);
        Procedure::create([
            'name' => 'Ecocardiograma',
            'value' => '300,00'
        ]);
        Procedure::create([
            'name' => 'Teste de Ergométrico',
            'value' => '150,00'
        ]);

        ProfessionalHasProcedure::create([
            'professional_id' => 1, 
            'procedure_id' => 1
        ]);
        ProfessionalHasProcedure::create([
            'professional_id' => 1,
            'procedure_id' => 2
        ]);
        ProfessionalHasProcedure::create([
            'professional_id' => 1,
            'procedure_id' => 3
        ]);

        // Ortopedia
        Procedure::create([
            'name' => 'Audiometria vocal',
            'value' => '500,00'
        ]);
        Procedure::create([
            'name' => 'Irrigação com bulbo',
            'value' => '100,00'
        ]);

        ProfessionalHasProcedure::create([
            'professional_id' => 2,
            'procedure_id' => 4
        ]);
        ProfessionalHasProcedure::create([
            'professional_id' => 2,
            'procedure_id' => 5
        ]);

        // Otorrinolaringologia
        Procedure::create([
            'name' => 'Densitometria Óssea',
            'value' => '400,00'
        ]);
        Procedure::create([
            'name' => 'Cintilografia Óssea',
            'value' => '200,00'
        ]);

        ProfessionalHasProcedure::create([
            'professional_id' => 3,
            'procedure_id' => 6
        ]);
        ProfessionalHasProcedure::create([
            'professional_id' => 3,
            'procedure_id' => 7
        ]);
    }
}
