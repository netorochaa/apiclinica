<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceHasProcedure extends Model
{
    use HasFactory;
    protected $table = 'attendance_has_procedures';
    protected $fillable = [
        'attendance_id', 
        'procedure_id'
    ];
}
