<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfessionalHasProcedure extends Model
{
    use HasFactory;
    protected $table = 'professional_has_procedures';
    protected $fillable = [
        'professional_id', 
        'procedure_id'
    ];
}
