<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value'
    ];

    /**
     * The roles that belong to the Procedure
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attendances()
    {
        return $this->belongsToMany(Attendance::class, 'attendance_has_procedures', 'procedure_id', 'attendance_id');
    }

    /**
     * The roles that belong to the Procedure
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function professionals()
    {
        return $this->belongsToMany(Professional::class, 'professional_has_procedures', 'procedure_id', 'professional_id');
    }
}
