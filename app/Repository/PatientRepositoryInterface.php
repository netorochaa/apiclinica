<?php

namespace App\Repository;

interface PatientRepositoryInterface
{

   public function save($request);

   public function find($id);

   public function findByCpf($cpf);

   public function getAll();
}