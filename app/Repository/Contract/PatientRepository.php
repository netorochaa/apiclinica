<?php

namespace App\Repository\Contract;

use App\Repository\PatientRepositoryInterface;
use App\Models\Patient;

/**
* Interface PatientRepositoryInterface
* @package App\Repositories
*/
class PatientRepository implements PatientRepositoryInterface
{    
    protected $patient;       

    public function __construct(Patient $patient)     
    {         
        $this->patient = $patient;
    }

    public function save($request)
    {
        return $this->patient->create($request);
    }

    public function find($id)
    {
        return $this->patient->find($id);
    }

    public function findByCpf($cpf)
    {
        return $this->patient->where('cpf', $cpf)->get();
    }

    public function getAll()
    {
        return Patient::all();
    }
}